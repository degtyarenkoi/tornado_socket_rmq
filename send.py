#!/usr/bin/env python
import pika
import logging


def rmq_producer():
    connection = pika.BlockingConnection(pika.ConnectionParameters(
            host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='hello')

    message = get_content()

    for i in range(100):
        channel.basic_publish(exchange='',
                          routing_key='hello',
                          body=message)
    print("Sent %s: " % message)
    logging.info("Sent to RMQ message: %s " % message)
    connection.close()


def get_content():
    with open("file.json") as f:
        return f.read()


if __name__ == "__main__":
    logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                        filename='logs.log', level=logging.INFO)

    rmq_producer()
