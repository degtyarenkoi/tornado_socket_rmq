import socket
import logging
import functools
from tornado.ioloop import IOLoop
from tornado.iostream import IOStream


def connection_ready(sock, fd, ev):
    while True:
      try:
        conn, addr = sock.accept()
      except socket.error as e:
#        print e
        pass
        break

    while True:
        data = conn.recv(1024)
        if not data:
          break
        print "client is at", addr, data

        with open("qwe") as f:
            count = f.read()
            count = int(count)
            count += 1
            with open("qwe", "w") as f:
                f.write(str(count))

        logging.info("client is at ", addr, data)

        conn.send(data)
#        Communicate(conn)
#        stream = IOStream(conn)
#        data = stream.read_bytes(1024)
#        print data


def srv_main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    host = "localhost"
    port = 9090
    sock.setblocking(0)
    sock.bind((host, port))
    sock.listen(100)

    io_loop = IOLoop.current()
    callback = functools.partial(connection_ready, sock)
    io_loop.add_handler(sock.fileno(), callback, io_loop.READ)
    io_loop.start()


if __name__ == "__main__":
  srv_main()