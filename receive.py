#!/usr/bin/env python
import pika
import logging
import socket
from multiprocessing import Pool
from tornado.iostream import IOStream


def get_message(pool):
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='hello')

    print '...waiting for message...'
    logging.info('...waiting for message...')

    def callback(ch, method, properties, body):
        print 'received %r' % (body,)
        logging.info('received from RMQ %r' % body)

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

 #       stream = IOStream(sock)
        host = "localhost"
        port = 9090

#        stream.connect((host, port))
        sock.connect((host, port))
#        stream.write(body)
        sock.send(body)

        print "sent to Tornado server: %r" % body
        logging.info("sent to Tornado server: %r" % body)

        try:
#            data = stream.read_bytes(1024)
            data = sock.recv(1024)
        except socket.error:
            print "there is no data yet"
        else:
            print "RECEIVED FROM TORNADO SERVER: ", data
            logging.info("received message: %r" % (data, ))

        sock.close()

    pool.apply_async(channel.basic_consume(callback, queue='hello'))
    channel.start_consuming()


if __name__ == "__main__":
    logging.basicConfig(format=u"%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s",
                        filename='logs.log', level=logging.INFO)
    pool = Pool(processes=2)
    get_message(pool)
